function map_init(lat, long) {
	var map = L.map('map').setView([lat, long], 5);

  var tiles = L.tileLayer(
    'http://stamen-tiles-d.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
    {
		maxZoom: 18,
		attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(map);

	var circle = L.circle([lat, long], {
		color: 'red',
		fillColor: '#f03',
		fillOpacity: 0.5,
		radius: 20
	}).addTo(map);

  window.map = map;
}

function map_add_entry(lat, long, content) {
  L.marker([lat, long])
   .addTo(window.map)
   .bindPopup(content).openPopup();
}
