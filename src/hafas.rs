static SERVER: &str = "https://v5.db.transport.rest";
use std::collections::HashMap;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug, Clone)]
/// Stop data returned by Hafas API
pub struct Stop {
    //r#type: String,
    pub id: String,
    pub name: String,
    pub location: Option<Location>,
    pub products: Option<Products>
}

#[derive(Deserialize, Debug, Clone)]
/// Location data returned by Hafas API
pub struct Location {
    //r#type: String,
    pub id: String,
    pub latitude: f64,
    pub longitude: f64
}
pub type Products = HashMap<String, bool>;

pub type GetLocationsResponse = Vec<Stop>;
pub async fn get_locations(query: String) -> Result<GetLocationsResponse, Box<dyn std::error::Error>> {
    let resp = reqwest::get(format!("{}/locations?poi=false&addresses=false&query={}", SERVER, query))
        .await?
        .json::<GetLocationsResponse>()
        .await?;

    Ok(resp)
}

#[derive(Deserialize, Debug, Clone)]
pub struct DurationCollection {
    pub duration: usize,
    pub stations: Vec<Stop>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReachableConfig {
    pub maxTransfers: usize,
    pub maxDuration: usize,
    pub nationalExpress: bool,
    pub national: bool,
    pub regionalExp: bool,
    pub regional:bool,
    pub suburban: bool,
    pub bus: bool,
    pub ferry: bool,
    pub subway: bool,
    pub tram: bool,
    pub taxi: bool
}
impl Default for ReachableConfig {
    fn default() -> Self {
        ReachableConfig {
            maxTransfers: 2048,
            maxDuration: 120,
            nationalExpress: false,
            national: false,
            regional: true,
            regionalExp: true,
            suburban: true,
            subway: true,
            bus: true,
            ferry: false,
            tram: true,
            taxi: false
        }
    }
}
pub type GetStopsReachableFromResponse = Vec<DurationCollection>;
pub async fn get_stops_reachable_from(
    lat: f64,
    long: f64,
    address: String,
    config: ReachableConfig
) -> Result<GetStopsReachableFromResponse, Box<dyn std::error::Error>> {
    let resp = reqwest::get(
        format!("{}/stops/reachable-from?latitude={}&longitude={}&address={}&{}",
                SERVER,
                lat,
                long,
                address,
                serde_qs::to_string(&config).unwrap_or_else(|_|String::from("language=en"))
        ))
        .await?
        .json::<GetStopsReachableFromResponse>()
        .await?;

    Ok(resp)
}
