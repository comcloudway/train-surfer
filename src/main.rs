#![feature(async_closure)]
use dioxus::prelude::*;
use wasm_bindgen::prelude::*;
use std::ops::BitXor;

mod hafas;
use hafas::{
    get_locations,
    get_stops_reachable_from
};

#[wasm_bindgen]
extern "C" {
    fn map_init(lat: f64, long: f64);
    fn map_add_entry(lat: f64, long: f64, content: String);
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    dioxus::web::launch(app);
}

enum LoadingMode {
    None,
    Wait,
    Error
}

fn app(cx: Scope) -> Element {

    let query = use_state(&cx, String::new);
    let stops = use_state(&cx, Vec::new);
    let station = use_state(&cx, || Option::<hafas::Stop>::None);
    let config = use_state(&cx, hafas::ReachableConfig::default);
    let results = use_state(&cx, Vec::<hafas::DurationCollection>::new);
    let use_map = use_state(&cx, || true);
    let lmode = use_state(&cx, || LoadingMode::None);

    let popup = match lmode.get() {
        LoadingMode::None => {
            None
        },
        LoadingMode::Wait => {
            // loading animation
            Some(rsx!(div {
                class: "bg-slate-100 fixed top-0 left-0 z-50 w-full h-full items-center justify-center absolute flex flex-col",
                div {
                    class: "rounded items-center flex p-5 max-w-[98vw] flex-col border border-slate-400 bg-slate-50 gap-4",
                    img {
                        class: "w-[20rem] m-0 animate-pulse",
                        src: "./public/icon.png"
                    }

                    p {
                        class: "font-semibold text-2xl m-0",
                        "Loading"
                    }
                }}))
        },
        LoadingMode::Error => {
            // show error screen
            Some(rsx!(div {
                class: "bg-slate-100 fixed top-0 left-0 z-50 w-full h-full items-center justify-center absolute flex flex-col",
                div {
                    class: "rounded items-center flex p-5 max-w-[98vw] flex-col border border-slate-400 bg-slate-50 gap-4",
                    img {
                        class: "w-[20rem] m-0 animate-pulse",
                        src: "./public/icon-broken.png"
                    }
                    p {
                        class: "font-semibold text-2xl m-0",
                        "Network requiest failed"
                    }
                    button {
                        class: "w-full button flex items-center justify-center whitespace-nowrap h-10 px-6 font-semibold rounded-md bg-black text-white hover:bg-transparent hover:text-black hover:border-black border border-black transition-colors duration-300",
                        onclick: move |_| {
                            lmode.set(LoadingMode::None);
                        },
                        "Close"
                    }

                }}))
        }
    };


    if let Some(stop) = station.get() {
        let lat = stop.location.as_ref().unwrap().latitude;
        let long = stop.location.as_ref().unwrap().longitude;
        cx.render(rsx! {
            popup.is_some().then(||popup.unwrap())

                           div {
                               class: "w-full h-full items-center justify-center absolute flex flex-col",

                               use_map.then(||rsx!(div {
                                   id: "map",
                                   class: "h-full w-[100vw] relative z-0",
                               })),

                               use_map.get().bitxor(true).then(||rsx!(div {
                                   /*list mode (better performance)*/
                                   class: "h-full flex flex-col w-full overflow-auto gap-4 p-2",

                                   results.iter().map(|set| rsx!( div {
                                       key: "{set.duration}",
                                       class: "flex flex-col divide-y",
                                       p {
                                           class: "font-semibold text-2xl m-1",
                                           "{set.duration}"
                                       }

                                       div {
                                           set.stations
                                              .iter().map(|station|rsx!(p{
                                                  key: "{station.id}",
                                                  "{station.name}"
                                              }))
                                       }
                                   } ))
                               })),

                               div {
                                   /* panel */
                                   class: "w-[100vw] h-[15rem] p-2 divide-y flex items-center justify-center",

                                   div {
                                       class: "h-full p-1 border rounded-md border-slate-300 bg-slate-50 flex flex-col items-center justify-center",
                                       p {
                                           class: "font-semibold text-xl mb-0 w-full",
                                           "{stop.name}"
                                       }
                                       p {
                                           class: "text-slate-700 m-1 mt-0 w-full",
                                           "{lat}, {long}"
                                       }


                                       div {
                                           class: "flex flex-col gap-4 w-full",

                                           div {
                                               class: "flex w-full justify-between",
                                               p {
                                                   "Use Map"
                                               }
                                               input {
                                                   class: "rounded",
                                                   "type": "checkbox",
                                                   checked: "{use_map}",
                                                   onchange: move |e| {
                                                       *use_map.make_mut()= e.value.parse::<bool>().unwrap_or(false);
                                                   }
                                               }
                                           }
                                           button {
                                               class: "w-full button flex items-center justify-center whitespace-nowrap h-10 px-6 font-semibold rounded-md bg-black text-white hover:bg-transparent hover:text-black hover:border-black border border-black transition-colors duration-300",
                                               onclick: move |_| cx.spawn({
                                                   let stop = stop.clone();
                                                   let config = config.clone();
                                                   let results = results.clone();
                                                   let use_map = use_map.clone();
                                                   let lmode = lmode.clone();
                                                   async move {
                                                       lmode.set(LoadingMode::Wait);
                                                       let res = get_stops_reachable_from(
                                                           lat,
                                                           long,
                                                           stop.name,
                                                           config.get().clone()).await;
                                                       if let Ok(list) = res {
                                                           if *use_map.get() {
                                                               map_init(
                                                                   stop.location.as_ref().unwrap().latitude,
                                                                   stop.location.as_ref().unwrap().longitude
                                                               );

                                                               for duration in list.iter() {
                                                                   for station in duration.stations.iter() {
                                                                       if let Some(location) = &station.location {
                                                                           map_add_entry(
                                                                               location.latitude,
                                                                               location.longitude,
                                                                               format!(
                                                                                   "{}: {}",
                                                                                   duration.duration,
                                                                                   station.name));
                                                                       }
                                                                   }
                                                               }


                                                           } else {
                                                               *results.make_mut() = list;
                                                           }
                                                           lmode.set(LoadingMode::None);

                                                       } else {
                                                           log::warn!("F***: {:?}", res);
                                                           lmode.set(LoadingMode::Error);
                                                       }
                                                   }
                                               }),
                                               "Apply"
                                           }
                                           button {
                                               class: "whitespace-nowrap text-sm text-gray-600 hover:text-gray-800 transition-colors duration-300",
                                               onclick: move |_| {
                                                   *station.make_mut() = None;
                                               },
                                               "Select a different Station"
                                           }
                                       }
                                   }

                                   div {
                                       /*tweaks*/
                                       class: "overflow-auto w-full h-full border rounded-md bg-slate-50  border-slate-300 m-1 p-1 flex flex-col items-center gap-4",

                                       div {
                                           class: "flex flex-col w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "How much time to spend in a train"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "number",
                                               min: "0",
                                               max: "600",
                                               value: "{config.maxDuration}",
                                               onchange: move |e| {
                                                   config.make_mut().maxDuration=e.value.parse().unwrap_or(0);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex flex-col w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "How often you want to transfer to a different train"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "number",
                                               min: "0",
                                               max: "600",
                                               value: "{config.maxTransfers}",
                                               onchange: move |e| {
                                                   config.make_mut().maxTransfers=e.value.parse().unwrap_or(0);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "ICE?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.nationalExpress}",
                                               onchange: move |e| {
                                                   config.make_mut().nationalExpress= e.value.parse().unwrap_or(false);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "IC/EC?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.national}",
                                               onchange: move |e| {
                                                   config.make_mut().national= e.value.parse().unwrap_or(false);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "RE/IR?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.regionalExp}",
                                               onchange: move |e| {
                                                   config.make_mut().regionalExp= e.value.parse().unwrap_or(true);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "RB?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.regional}",
                                               onchange: move |e| {
                                                   config.make_mut().regional= e.value.parse().unwrap_or(true);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "S-Bahn?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.suburban}",
                                               onchange: move |e| {
                                                   config.make_mut().suburban= e.value.parse().unwrap_or(true);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "Bus?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.bus}",
                                               onchange: move |e| {
                                                   config.make_mut().bus= e.value.parse().unwrap_or(true);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "Ferry?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.ferry}",
                                               onchange: move |e| {
                                                   config.make_mut().ferry= e.value.parse().unwrap_or(false);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "U-Bahn?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.subway}",
                                               onchange: move |e| {
                                                   config.make_mut().subway= e.value.parse().unwrap_or(true);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "Tram?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.tram}",
                                               onchange: move |e| {
                                                   config.make_mut().tram= e.value.parse().unwrap_or(true);
                                               }
                                           }
                                       }
                                       div {
                                           class: "flex w-full gap-4 justify-between items-center px-2",
                                           p {
                                               "Group Taxi?"
                                           }
                                           input {
                                               class: "rounded",
                                               "type": "checkbox",
                                               checked: "{config.taxi}",
                                               onchange: move |e| {
                                                   config.make_mut().taxi= e.value.parse().unwrap_or(false);
                                               }
                                           }
                                       }
                                   }
                               }
                           }
        })
    } else {
        cx.render(rsx! {
            popup.is_some().then(||popup.unwrap())
                           div {
                               class: "w-full h-full items-center justify-center absolute flex flex-col",
                               div {
                                   class: "rounded items-center flex p-5 max-w-[98vw] flex-col border border-slate-400 bg-slate-50 gap-4",

                                   img {
                                       class: "w-[15rem] m-0",
                                       src: "./public/icon.png"
                                   }

                                   p {
                                       class: "font-semibold text-4xl mb-8",
                                       "Select a train station"
                                   }

                                   form {
                                       prevent_default: "onsubmit",
                                       class: "flex gap-4 w-full",
                                       onsubmit: move |_| cx.spawn({
                                           let query = query.get().to_string();
                                           let stops = stops.clone();
                                           let lmode = lmode.clone();
                                           async move {
                                               lmode.set(LoadingMode::Wait);
                                               let res = get_locations(query).await;
                                               if let Ok(list) = res {
                                                   stops.set(list);
                                                   lmode.set(LoadingMode::None);
                                               } else {
                                                   lmode.set(LoadingMode::Error);
                                                   log::warn!("F***: {:?}", res);
                                               }
                                           }
                                       }),
                                       /* search box */
                                       input {
                                           class: "p-2 border w-[inherit] rounded-md border-slate-300 shadow-sm focus:border-slate-800 focus:ring focus:ring-slate-200 focus:ring-opacity-50 ring-0",
                                           name: "location",
                                           oninput: move |evt| query.set(evt.value.clone()),
                                           "type": "text",
                                           placeholder: "Where do you want to start your trip?",
                                           value: "{query}"
                                       }
                                       input {
                                           "type": "submit",
                                           class: "button flex items-center justify-center whitespace-nowrap h-10 px-6 font-semibold rounded-md bg-black text-white hover:bg-transparent hover:text-black hover:border-black border border-black transition-colors duration-300",
                                           value: "Search"
                                       }
                                   }
                                   div {
                                       /* search results */
                                       class: "flex flex-col gap-4 border w-full p-2 py-4 rounded border-slate-300",
                                       stops.is_empty().then(|| rsx! {
                                           p {
                                               class: "text-slate-700 m-1",
                                               "Insert a location and click search to get a list of available stations"
                                           }
                                       })
                                                       stops
                                           .iter()
                                           .filter(|stop| stop.location.is_some())
                                           .map(|stop| {
                                               let lat = stop.location.as_ref().unwrap().latitude;
                                               let long = stop.location.as_ref().unwrap().longitude;
                                               rsx!{
                                                   div {
                                                       class: "flex gap-8 border rounded items-center w-full p-1",
                                                       key: "{stop.id}",

                                                       div {
                                                           class: "flex flex-col w-full",
                                                           p {
                                                               class: "font-semibold text-xl",
                                                               "{stop.name}"
                                                           }
                                                           p {
                                                               class: "text-slate-700 m-1",
                                                               "{lat}, {long}"
                                                           }
                                                       }

                                                       button {
                                                           class: "whitespace-nowrap h-10 px-6 font-semibold rounded-md bg-transparent text-gray-600 hover:bg-gray-800 hover:text-white hover:border-gray-800 border border-gray-600 transition-colors duration-300",
                                                           onclick: move |_| {
                                                               *station.make_mut()=Some(stop.clone());
                                                           },
                                                           "Select"
                                                       }
                                                   }
                                               }
                                           })
                                   }
                               }
                           }
        })
    }

}
